package com.baomidou.mybatisplus.test.query;

import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.plugin.Interceptor;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import com.baomidou.mybatisplus.annotation.Relation;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaJoinQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.JoinInterceptor;
import com.baomidou.mybatisplus.test.BaseDbTest;

import lombok.Data;

/**
 * @author mahuibo
 * @since 2022/11/24
 */
public class LambdaJoinQueryWrapperTest extends BaseDbTest<LambdaJoinQueryWrapperTest.TableMapper> {

    @Test
    void testLambdaOrderBySqlSegment() {
//        TableInfo tableInfo = TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), LambdaJoinQueryWrapperTest.TableOne.class);
//        Assertions.assertEquals("xxx", tableInfo.getTableName());
        LambdaJoinQueryWrapper<LambdaJoinQueryWrapperTest.TableOne> lqw = new LambdaJoinQueryWrapper<>(LambdaJoinQueryWrapperTest.TableOne.class);
//        LambdaQueryWrapper<TableOne> lqw = new LambdaQueryWrapper<>(LambdaJoinQueryWrapperTest.TableOne.class);
//        QueryJoinWrapper<LambdaJoinQueryWrapperTest.TableOne> wrapper = new QueryJoinWrapper<>(LambdaJoinQueryWrapperTest.TableOne.class);
        doTest(i -> {
//            wrapper.leftJoin(TableTwo.class)
//                .eq("table_two_id", 1)
//                .select("name")
//            ;
            /*
                left join table_two as tableTwo on  tableTwo.table_two_id = master.id
                where tableTwo.table_two_id = 1
             */
            lqw
//                .innerJoin(TableTwo.class, TableTwo::getTableTwoId, TableOne::getTableTwoId, w -> {
//                    return w.eq(TableTwo::getName, "name222222222");
////                    return w;
//                });
                .leftJoin(TableTwo.class, w -> w.eq(TableTwo::getTableTwoId, 1));

//                .eq(TableTwo::getTableTwoId,TableOne::getTableTwoId);
//                .selectFun(DefaultFuncEnum.COUNT,TableOne::getCountName)
//                .leftJoin(TableTwo.class, TableTwo::getTableTwoId, TableOne::getTableTwoId)
//                .select(TableTwo::getName, TableTwo::getTableTwoId);
//            i.selectList(lqw);
            System.out.println(i.selectList(lqw));
        });

//        System.out.println(lqw.getSqlSegment());
//        System.out.println(lqw.getSqlSelect());
//        System.out.println(lqw.getFrom());
    }

    protected List<Interceptor> interceptors() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new JoinInterceptor());
        return Lists.newArrayList(interceptor);
    }

    @Override
    protected List<String> tableSql() {
        return Arrays.asList("drop table if exists xxx", "CREATE TABLE IF NOT EXISTS xxx (" +
                "id BIGINT NOT NULL," +
                "name VARCHAR(300) NULL DEFAULT NULL," +
                "table_two_id BIGINT NULL DEFAULT NULL," +
                "PRIMARY KEY (id))",
            "CREATE TABLE IF NOT EXISTS xxx2 (" +
                "table_two_id BIGINT NOT NULL," +
                "name VARCHAR(300) NULL DEFAULT NULL," +
                "PRIMARY KEY (table_two_id))");
    }


    @Override
    protected String tableDataSql() {
        return "insert into xxx(id,name,table_two_id) values(1,'name',1),(2,'name22',1);" +
            "insert into xxx2(table_two_id,name) values(1,'name222222222');";
    }


    @Data
    @TableName(value = "xxx", autoResultMap = true)
    public static class TableOne {

        @TableId("id")
        private Long id;

        @TableField("name")
        private String name;

        @TableField(value = "table_two_id", target = TableTwo.class)
        private Long tableTwoId;

        @TableField(funField = true, exist = false)
        private Integer countName;

        //指定关联关系，和目标类  如果是  onetoone 则不需要指定target
        @TableField(relation = Relation.ONE_TO_MANY, target = TableTwo.class, targetFields = "tableTwoId")
        List<TableTwo> tableTwos;
    }

    @Data
    @TableName(value = "xxx2", autoResultMap = true)
    public static class TableTwo {

        @TableId(value = "table_two_id")
        private Long tableTwoId;

        @TableField("name")
        private String name;
    }

    public interface TableMapper extends BaseMapper<TableOne> {

    }
}
