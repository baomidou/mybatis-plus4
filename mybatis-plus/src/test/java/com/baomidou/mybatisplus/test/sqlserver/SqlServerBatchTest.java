package com.baomidou.mybatisplus.test.sqlserver;

import com.baomidou.mybatisplus.test.sqlserver.entity.TestUser;
import com.baomidou.mybatisplus.test.sqlserver.mapper.TestUserMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * SQL Server数据库批量操作测试类
 * @author sundongkai
 * @since 2022-04-03
 */
@DirtiesContext
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"classpath:sqlserver/spring-test-sqlserver.xml"})
public class SqlServerBatchTest {

    @Resource
    private TestUserMapper testUserMapper;

    /**
     * 批量插入
     */
    @Test
    @Order(1)
    public void insertBatchTest() {
        TestUser testUser1 = new TestUser();
        testUser1.setId(1);
        testUser1.setName("小王");
        testUser1.setPhone("138xxxxxxxx");

        TestUser testUser2 = new TestUser();
        testUser2.setId(2);
        testUser2.setName("小李");
        testUser2.setPhone("136xxxxxxxx");

        TestUser testUser3 = new TestUser();
        testUser3.setId(3);
        testUser3.setName("小张");
        testUser3.setPhone("150xxxxxxxx");

        List<TestUser> testUserList = Arrays.asList(testUser1, testUser2, testUser3);

        testUserMapper.insertBatch(testUserList);
    }
}
